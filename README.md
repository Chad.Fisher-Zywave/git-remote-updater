# Git Remote Updater
Updates remote URLs of all git repositories in a provided path from gitlab.zywave.com to gitlab.com/zywave


## How-To

- Download [update-remotes.ps1](https://gitlab.com/Chad.Fisher-Zywave/git-remote-updater/-/raw/main/update-remotes.ps1?inline=false)
- Run in powershell providing the path to the directory containing one or more repositories you want to update the remotes on. Ex `.\update-remotes c:\dev\devsecops`


## Note
GitLab offers redirection for the git repositories of projects that have been moved. 
The redirects that exist on gitlab.zywave.com do not exist on gitlab.com. 
If your project changed location in gitlab.zywave.com and your remote URL was never updated you will notice it no longer works at all. 

To manually update in this scenario:
- Get the current git URL from your project in gitlab.com
- Within a terminal running within your local repo's directory run `git remote set-url origin <url>`.
