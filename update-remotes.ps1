param(
    [Parameter(Position=0, Mandatory =$true)]
    [String]$dir
)

if(-not (Test-Path $dir)) {
    Write-Error "Path not found: $dir"
    return
}

$folders = Get-ChildItem $dir -Recurse -Directory -Hidden | Where-Object { $_.Name.Contains('.git') }

foreach($folder in $folders) {
    Write-Host('Processing repository ' + $folder.Parent.FullName)
    Set-Location -Path $folder.FullName
    $originUrl = git config --get remote.origin.url
    if($originUrl.Contains('gitlab.zywave.com')) {
        if($originUrl.ToLower().StartsWith('http')) {
            $newOriginUrl = $originUrl.Replace('gitlab.zywave.com/', 'gitlab.com/zywave/')
        }
        else {
            $newOriginUrl = $originUrl.Replace('gitlab.zywave.com:', 'gitlab.com:zywave/')
        }
        
        Write-Host "Replaced origin $originUrl with $newOriginUrl"
        git remote set-url origin $newOriginUrl
    }
}
